module Main (main) where

-- Prelude used with Keid, from `rio` package
import RIO -- rio

-- From `rio-app` package
import Engine.App (engineMain)

-- Initial stage, from project library or executable
import Stage.StageName (stackStage)

main :: IO ()
main = engineMain stackStage
