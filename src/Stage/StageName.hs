module Stage.StageName where

-- the prelude
import RIO

import Engine.Stage.Component qualified as Stage
import Engine.Types (StackStage(..))

import Global.Render qualified as Render
import Stage.StageName.Resources qualified as Resources
import Stage.StageName.Scene qualified as Scene
import Stage.StageName.Types (Stage)

stackStage :: StackStage
stackStage = StackStage stage

stage :: Stage
stage =
  Stage.assemble
    "Main"
    Render.component
    Resources.component
    (Just Scene.component)
