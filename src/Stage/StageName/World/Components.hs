module Stage.StageName.World.Components where

import Prelude

import Apecs
import Geomancy

newtype Position = Position Vec3
  deriving (Eq, Show)

instance Component Position where
  type Storage Position = Map Position

