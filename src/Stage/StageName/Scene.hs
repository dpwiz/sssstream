module Stage.StageName.Scene where

import RIO

import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Render.DescSets.Set0 qualified as Set0
import Render.Draw qualified as Draw
import Render.Pass (usePass)
import Resource.Buffer qualified as Buffer
import RIO.State (gets)
import UnliftIO.Resource (ResourceT)

import Global.Resource.Assets qualified as Assets
import Stage.StageName.Types qualified as StageName
import Stage.StageName.Types (RunState(..))
import Stage.StageName.Events qualified as Events

component :: StageName.Scene
component = Stage.Scene
  { scBeforeLoop = beforeLoop
  , scUpdateBuffers = updateBuffers
  , scRecordCommands = recordCommands
  }

beforeLoop :: ResourceT (Engine.StageRIO RunState) ()
beforeLoop = do
  void $! Events.setup

updateBuffers :: StageName.UpdateBuffers ()
updateBuffers StageName.RunState{..} StageName.FrameResources{..} = do
  Scene.observe rsSceneWorld frSceneWorld
  Scene.observe rsSceneUI frSceneUI
  Buffer.observeCoherentResize_ rsStuff frStuff

recordCommands :: StageName.RecordCommands ()
recordCommands cb StageName.FrameResources{..} imageIndex = do
  Engine.Frame{fRenderpass, fSwapchainResources, fPipelines} <- asks snd
  assets <- gets rsAssets
  usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    Set0.withBoundSet0 frSceneWorld fPipelines.pUnlitColored cb do
      Graphics.bind cb fPipelines.pUnlitColored do
        Draw.indexed cb assets.models.sphere assets.models.zeroTransform

        stuff <- Worker.readObservedIO frStuff
        Draw.indexed cb assets.models.sphere stuff
