module Stage.StageName.World where

import Prelude
import Apecs

import Stage.StageName.World.Components

makeWorld "World"
  [ ''Position
  ]
