module Stage.StageName.Types where

import RIO

import Render.DescSets.Set0 qualified as Set0
import Vulkan.Core10 qualified as Vk
import Engine.Stage.Component qualified as Stage
import Engine.Worker qualified as Worker
import Engine.Camera qualified as Camera
import Resource.Buffer qualified as Buffer
import RIO.Vector.Storable qualified as Storable
import Geomancy (Transform)

import Global.Render qualified as Render
import Global.Resource.Assets (Assets)
import Stage.StageName.World (World)

type Stage = Render.Stage FrameResources RunState
type Frame = Render.Frame FrameResources
type Scene = Render.StageScene RunState FrameResources
type RecordCommands a = Vk.CommandBuffer -> FrameResources -> Word32 -> Render.StageFrameRIO FrameResources RunState a

type Resources = Stage.Resources Render.RenderPasses Render.Pipelines RunState FrameResources

type UpdateBuffers a = RunState -> FrameResources -> Render.StageFrameRIO FrameResources RunState a

-- Double-buffered resources used in rendering
data FrameResources = FrameResources
  { frSceneUI :: Set0.FrameResource '[Set0.Scene]
  , frSceneWorld :: Set0.FrameResource '[Set0.Scene]

  , frStuff :: Buffer.ObserverCoherent Transform
  }

-- Inter-frame persistent resources
data RunState = RunState
  { rsAssets :: Assets
  , rsSceneUI :: Set0.Process
  , rsSceneWorld :: Set0.Process
  , rsWorldView :: Worker.Var Camera.View

  , rsWorld :: World

  , rsStuff :: Worker.Var (Storable.Vector Transform)
  }
