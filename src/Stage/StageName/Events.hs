module Stage.StageName.Events where

import RIO

import Apecs qualified
import Engine.ReactiveBanana qualified as ReactiveBanana
import Engine.ReactiveBanana.Timer qualified as Timer
import Engine.Camera qualified as Camera
import Engine.Camera.Controls qualified as CameraControls
import Reactive.Banana qualified as RB
import Reactive.Banana.Frameworks qualified as RBF
import Engine.Types (StageRIO)
import UnliftIO.Resource (ResourceT)
import RIO.State (gets)
import GHC.Float (double2Float)
import Geomancy
import Geomancy.Transform qualified as Transform
import RIO.Vector.Storable qualified as Storable
import Engine.Worker qualified as Worker

import Stage.StageName.Types (RunState(..))
import Stage.StageName.World.Components qualified as World

setup :: ResourceT (StageRIO RunState) RBF.EventNetwork
setup = do
  RunState{..} <- gets id

  mkCameraUpdates <- Timer.every 10e3

  mkStuffUpdates <- Timer.every 10e3

  ReactiveBanana.allocateActuated \(UnliftIO unlift) _started -> do
    updateCameraE <- fmap (fmap double2Float) mkCameraUpdates

    updateStuffE <- fmap (fmap double2Float) mkStuffUpdates

    let
      cameraE =
        updateCameraE <&> \time ->
          Camera.initialOrbitalInput
            -- { Camera.orbitAzimuth = time / 2.0
            -- , Camera.orbitDistance = 1.5
            -- }

    let
      worldViewE =
        fmap Camera.mkViewOrbital_ cameraE

    ReactiveBanana.pushWorkerOutput rsWorldView worldViewE
    -- ReactiveBanana.reactimateDebugShow unlift worldViewE

    stuffTimeE <- RB.accumE 0 $
      updateStuffE <&> \_mono acc ->
        acc + 0.01

    RBF.reactimate $
      _started <&> \() ->
        Apecs.runWith rsWorld do
          void $! Apecs.newEntity $
            (World.Position $ vec3 0 0 2)

    RBF.reactimate $
      stuffTimeE <&> \_time ->
        Apecs.runWith rsWorld do
          -- Run updates
          Apecs.cmap \(World.Position pos) ->
            World.Position $ pos + vec3 0 0 0.001

          -- Do "rendering"
          -- i.e. prepare GPU buffers
          instances <- collect \(World.Position pos) ->
            Transform.scale 0.1 <>
            Transform.translateV pos
          -- XXX: batched per render pipeline
          Worker.pushOutput rsStuff \_old ->
            Storable.fromList instances

collect
  :: forall components w m a
  .  ( Apecs.Get w m components
     , Apecs.Members w m components
     )
  => (components -> a)
  -> Apecs.SystemT w m [a]
collect f = Apecs.cfold (\acc comps -> f comps : acc) []

collectMaybe
  :: forall components w m a
  .  ( Apecs.Get w m components
     , Apecs.Members w m components
     )
  => (components -> Maybe a)
  -> Apecs.SystemT w m [a]
collectMaybe f = Apecs.cfold (\acc -> maybe acc (: acc) . f) []
