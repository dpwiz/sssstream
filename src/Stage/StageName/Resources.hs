module Stage.StageName.Resources where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Engine.Camera qualified as Camera
import Engine.Camera.Controls qualified as CameraControls
import Engine.Stage.Component qualified as Stage
import Engine.Types (StageRIO, StageSetupRIO)
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Resource.CommandBuffer (withPools)
import Resource.Region (ReleaseKey)
import Resource.Region qualified as Region
import Resource.Buffer qualified as Buffer
import RIO.State (gets)
import Vulkan.Core10 qualified as Vk

import Global.Render qualified
import Global.Resource.Assets qualified as Assets
import Stage.StageName.Types qualified as StageName
import Stage.StageName.World (initWorld)

component :: StageName.Resources
component = Stage.Resources
  { rInitialRS = initialRunState
  , rInitialRR = initialRecycledResources
  }

initialRunState :: StageSetupRIO (ReleaseKey, StageName.RunState)
initialRunState = withPools \pools -> Region.run do
  -- Or, use a loader stage and request from arguments.
  rsAssets <- Assets.load pools

  ortho <- Camera.spawnOrthoPixelsCentered
  rsSceneUI <- Worker.spawnMerge1 mkSceneUi ortho

  perspective <- Camera.spawnPerspective
  -- rsViewP <- CameraControls.spawnViewOrbital Camera.initialOrbitalInput
  -- rsCameraControls <- CameraControls.spawnControls rsViewP
  rsWorldView <- Worker.newVar (Camera.mkViewOrbital_ Camera.initialOrbitalInput)
  rsSceneWorld <- Worker.spawnMerge2 mkScene perspective rsWorldView

  rsWorld <- liftIO initWorld

  rsStuff <- Worker.newVar mempty

  pure StageName.RunState{..}

mkScene :: Camera.Projection 'Camera.Perspective -> Camera.View -> Scene.Scene
mkScene Camera.Projection{..} Camera.View{..} = Scene.Scene{..}
  where
    sceneProjection    = projectionTransform
    sceneInvProjection = projectionInverse

    sceneView          = viewTransform
    sceneInvView       = viewTransformInv
    sceneViewPos       = viewPosition
    sceneViewDir       = viewDirection

    sceneFog           = 0
    sceneEnvCube       = 0 -- CubeMap.clouds CubeMap.indices
    sceneNumLights     = 0 -- fromIntegral $ Storable.length $ snd staticLights
    sceneTweaks        = 0

mkSceneUi :: Camera.Projection 'Camera.Orthographic -> Scene.Scene
mkSceneUi Camera.Projection{..} =
  Scene.emptyScene
    { Scene.sceneProjection    = projectionTransform
    , Scene.sceneInvProjection = projectionInverse
    }

initialRecycledResources
  :: Queues Vk.CommandPool
  -> Global.Render.RenderPasses
  -> Global.Render.Pipelines
  -> ResourceT (StageRIO StageName.RunState) StageName.FrameResources
initialRecycledResources _pools _renderpasses pipelines = do
  -- assets <- gets StageName.rsAssets
  -- let combined = fmap snd assets.aCombined

  frSceneUI <- Scene.allocate
    pipelines.pSceneLayout
    Nothing
    Nothing
    Nothing
    mempty
    Nothing

  frSceneWorld <- Scene.allocate
    pipelines.pSceneLayout
    Nothing
    Nothing
    Nothing
    mempty
    Nothing

  frStuff <- Buffer.newObserverCoherent
    "Stuff"
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    100
    mempty

  pure StageName.FrameResources{..}
