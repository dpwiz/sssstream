module Global.Resource.Assets where

import RIO

import Engine.Types (StageRIO)
import Engine.Vulkan.Types (Queues)
import Geomancy (Transform, Vec3)
import Geomancy.Vec3 qualified as Vec3
import Geomancy.Vec4 qualified as Vec4
import Render.Unlit.Colored.Model qualified as UnlitColored
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import Data.Vector.Storable qualified as Storable
import Geometry.Icosphere qualified as Icosphere
import Vulkan.Core10 qualified as Vk
import UnliftIO.Resource (ResourceT)

data Assets = Assets
  { models :: Models
  }

data Models = Models
  { sphere :: UnlitColored.Model 'Buffer.Staged
  , zeroTransform :: Buffer.Allocated 'Buffer.Staged Transform
  }

load
  :: Queues Vk.CommandPool
  -> ResourceT (StageRIO rs) Assets
load pools = do
  let (positions, attrs, indices) = someSphere
  sphere <- Model.createStaged (Just "sphere") pools positions attrs indices
  Model.registerIndexed_ sphere

  zeroTransform <- Buffer.createStaged
    (Just "zeroTransform")
    pools
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    [mempty]
  void $! Buffer.register zeroTransform

  let models = Models{..}

  pure Assets{..}

someSphere
  :: ( Storable.Vector Vec3.Packed
     , Storable.Vector UnlitColored.VertexAttrs
     , Storable.Vector Word32
     )
someSphere =
  Icosphere.generateIndexed
    3
    mkInitialAttrs
    mkMidpointAttrs
    mkVertices
  where
    mkInitialAttrs :: Vec3 -> ()
    mkInitialAttrs _pos = ()

    mkMidpointAttrs :: Float -> Vec3 -> () -> () -> ()
    mkMidpointAttrs _scale _midPos _attr1 _attr2 = ()

    mkVertices points _faces = do
      (rawPos, ()) <- points
      let normPos = Vec3.normalize rawPos
      pure
        ( Vec3.Packed normPos
        , Vec4.fromVec3 (normPos / 2 + 0.5) 1
        )
